const express = require('express');
const router = express.Router();
const auth = require('../auth');

const courseControllers = require('../controllers/courseController');

const { verify, verifyAdmin } = auth;

//Upon verifying that the user is logged in and is the value of isAdmin is true then execute the controller
router.post('/', verify, verifyAdmin, courseControllers.addCourse);
router.get('/', courseControllers.getAllCourses);
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);
//updating a course via ID
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

router.put(
  '/archive/:id',
  verify,
  verifyAdmin,
  courseControllers.archiveCourse
);

router.put('/activate/:id', courseControllers.activateCourse);

router.get('/getActiveCourses', courseControllers.getActiveCourse);

module.exports = router;
